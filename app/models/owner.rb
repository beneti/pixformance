class Owner < ActiveRecord::Base
	has_one :user, as: :member
	has_one :club
end
