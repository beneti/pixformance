class Club < ActiveRecord::Base
	belongs_to :owner
	has_many :members
	validates :name, presence: true
end
