class Member < ActiveRecord::Base
	has_one :user, as: :member
	belongs_to :club
end
