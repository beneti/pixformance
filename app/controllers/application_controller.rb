class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  helper_method :current_user

  def is_admin
    current_user && current_user.member_type == 'Owner'
  end
  helper_method :is_admin

  def is_admin_logged?
  	redirect_to new_session_path if !is_admin
  end

  def is_logged?
  	redirect_to new_session_path if !current_user
  end
end
