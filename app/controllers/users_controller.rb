class UsersController < ApplicationController
	def new
		@user = User.new
	end

  def create
  	@member = params[:user][:member].classify.constantize.create!

  	@user = User.new(user_params)
  	@user.member = @member
  	@user.save!
  	
  end

  private
    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation)
    end
end
