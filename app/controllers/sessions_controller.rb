class SessionsController < ApplicationController
	def new
  end

  def create
    user = User.find_by(email: params[:email])
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      session[:member_type] = user.member_type
      redirect_to new_club_path, :notice => "Logged in!"
    else
      flash.now.alert = "Invalid email or password"
      render "new"
    end
  end

  def destroy
    session[:user_id] = nil
    session[:member_type] = nil
    redirect_to new_session_path, :notice => "Logged out!"
  end
end
