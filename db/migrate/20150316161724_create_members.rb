class CreateMembers < ActiveRecord::Migration
  def change
    create_table :members do |t|
      t.string :last_name
      t.string :first_name
      t.datetime :birthday

      t.timestamps null: false
    end
  end
end
