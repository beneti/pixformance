class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email
      t.string :password_digest
      t.integer :member_id
      t.string :member_type

      t.timestamps null: false
    end
  end
end
