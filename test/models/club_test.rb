require 'test_helper'

class ClubTest < ActiveSupport::TestCase
  def test_create_invalid_club
		c = Club.new
		assert !c.valid?
	end

	def test_create_valid_club
		c = clubs(:one)
		assert c.valid?
	end

	def test_get_owner
		c = clubs(:one)
		o = owners(:one)
		c.owner = o
		assert c.valid?
	end

	def test_get_members_list
		c = clubs(:one)
		m = members(:one)
		c.members << m
		assert_equal c.members.first, m
	end
end
