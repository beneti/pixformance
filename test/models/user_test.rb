require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def test_create_invalid_user
		u = users(:one)
		assert !u.valid?
	end

	def test_create_valid_user
		u = users(:one)
		u.password = "pass"
		u.password_confirmation = "pass"
		assert u.valid?
	end
end
