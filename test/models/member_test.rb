require 'test_helper'

class MemberTest < ActiveSupport::TestCase
  def test_create_valid_member
		u = users(:one)
		o = members(:one)
		u.password = "pass"
		u.password_confirmation = "pass"
		u.member = o
		assert u.valid?
		assert_equal u.member_type, "Member"
	end
end
