require 'test_helper'

class OwnerTest < ActiveSupport::TestCase
  def test_create_valid_owner
		u = users(:one)
		o = owners(:one)
		u.password = "pass"
		u.password_confirmation = "pass"
		u.member = o
		assert u.valid?
		assert_equal u.member_type, "Owner"
	end
end
